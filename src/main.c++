#include <fstream>
#include <iostream>

#include "sorter.h++"

int main(int argc, char **argv) {
  // speed up cout and cin
  std::ios_base::sync_with_stdio(false);

  if (argc != 3) {
    std::cout << "Usage " << argv[0] << " <input file name> <output file name>"
              << std::endl;
    return 0;
  }

  std::ifstream in{argv[1]};
  std::ofstream out{argv[2]};

  if (!in.is_open()) {
    std::cout << "Failed to open input file" << std::endl;
    return 0;
  }

  if (!out.is_open()) {
    std::cout << "Failed to open output file" << std::endl;
    return 0;
  }

  Sorter sorter;
  sorter.load(in);
  sorter.sort();
  sorter.write(out);

  double a;
  //  std::cin >> a;

  return 0;
}
