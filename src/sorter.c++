#include "sorter.h++"
#include <bits/stdc++.h>
#include <filesystem>
#include <iostream>
#include <math.h>

//#define DEBUG_MODE

Sorter::~Sorter() {
  for (auto &stream : streams) {
    stream.free();
    stream.delete_();
  }
}

void Sorter::load(std::ifstream &in) {
  std::cout << "Loading..." << std::endl;
  double current{};

  streams.reserve(parts);

  for (size_t i = 0; i < parts; i++) {
    streams.push_back({int(i), current});
  }

  long counter = 0;

  do {
    in >> current;

    if (in.eof()) {
      break;
    }

    streams[counter % parts].setLastValue(current);

    //    if ((counter & 0xfff) == 0x0)
    //      std::cout << counter << "/89434796\r";

    ++counter;
  } while (!in.eof());

  //  for (auto &stream : streams) {
  //    //
  //  }
}

void Sorter::sort() {
  for (size_t i = 0; i < parts; i++) {
    std::cout << "Sorting... " << i << " of " << parts << std::endl;
    auto [arr, size] = streams[i].getAllContent();

    std::sort(arr, arr + size);

    streams[i].writeRawData(arr, size);
    delete arr;
  }
}

void Sorter::write(std::ofstream &out) {
  std::cout << "Merging..." << std::endl;
  for (auto &stream : streams) {
    stream.prepareForMergeSort();
    stream.readValue();
  }

  while (streams.size() > 0) {
    size_t min_index = 0;
    double min = streams[0].getLastValue();

    for (size_t i = 0; i < streams.size(); i++) {
      if (streams[i].getLowValue() < min) {
        min_index = i;
      }
    }

    out << min << std::endl;

    if (!streams[min_index].readValue()) {
      streams.erase(streams.begin() + min_index);
    }
  }
}

CacheUnit::CacheUnit(int id, double value) {
  filename = ".sf282w982lkw_cache_unit_" + std::to_string(id);
  file = new std::fstream(filename, std::ios_base::out | std::ios_base::binary);
  revese = new std::fstream(filename + "_reverse",
                            std::ios_base::out | std::ios_base::binary);
  //  std::cout << "new cache unit created: " << filename << std::endl;
  //  setLowValue(value);
  //  setLastValue(value);
  lastValue.digits = lowValue.digits = value;
}

CacheUnit::CacheUnit(const CacheUnit &copy) {
  lowValue = copy.lowValue;
  lastValue = copy.lastValue;
  file = copy.file;
  revese = copy.revese;
  filename = copy.filename;
  //  std::cout << "copied: " << filename << std::endl;
}

void CacheUnit::free() {
  file->close();
  delete file;
  revese->close();
  delete revese;
}

void CacheUnit::delete_() {
  std::filesystem::remove(filename);
  std::filesystem::remove(filename + "_reverse");
}

Array CacheUnit::getAllContent() {
  file->close();
  file->open(filename, std::ios_base::in | std::ios_base::binary);
  file->seekg(0, file->end);

  size_t size = file->tellg() / sizeof(double);
  double *ret = new double[size];

  file->seekg(0, file->beg);
  file->read(reinterpret_cast<char *>(ret), size * sizeof(double));

  return {ret, size};
}

void CacheUnit::writeRawData(double *arr, size_t size) {
  revese->write(reinterpret_cast<char *>(arr), size * sizeof(double));
  revese->flush();
}

CacheUnit &CacheUnit::operator=(const CacheUnit &other) {
  lastValue = other.lastValue;
  lowValue = other.lowValue;
  file = other.file;
  revese = other.revese;
  filename = other.filename;
  return *this;
}

bool CacheUnit::operator>(double value) { return lastValue.digits > value; }

void CacheUnit::setLastValue(double value) {
  lastValue.digits = value;
#ifdef DEBUG_MODE
  *file << value << std::endl;
#else
  file->write(lastValue.chars, sizeof(double));
#endif
  file->flush();
}

void CacheUnit::setLowValue(double value) {
  lowValue.digits = value;
#ifdef DEBUG_MODE
  *revese << value << std::endl;
#else
  revese->write(lowValue.chars, sizeof(double));
#endif
  revese->flush();
}

void CacheUnit::switchToReadMode() {
  //  file->close();
  //  file->open(filename, std::ios_base::in | std::ios_base::binary);
  //  file->seekg(0);
  //  revese->close();
  //  revese->open(filename + "_reverse",
  //               std::ios_base::in | std::ios_base::binary);
  //  revese->seekg(0, revese->end);
}

void CacheUnit::prepareForMergeSort() {
  revese->close();
  revese->open(filename + "_reverse",
               std::ios_base::in | std::ios_base::binary);
  revese->seekg(0, revese->beg);
}

bool CacheUnit::readValue() {
  //  if (revese->tellg() == 0) {
  revese->read(lastValue.chars, sizeof(double));

  if (revese->eof()) {
    return false;
  }
  return true;
  //  }
  //  return false;
  /*else {
    long pos = revese->tellg() - 4l;

    revese->seekg(pos);
    revese->read(lastValue.chars, sizeof(double));
    return true;
  }*/
}

double CacheUnit::getLastValue() { return lastValue.digits; }

double CacheUnit::getLowValue() { return lowValue.digits; }
