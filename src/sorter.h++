#pragma once

#include <fstream>
#include <vector>

union adouble {
  double digits;
  char chars[sizeof(double)];
};

struct Array {
  double *arr = nullptr;
  size_t size = 0;
};

class CacheUnit {
  std::fstream *file;   ///< keep all values
  std::fstream *revese; ///< keep all values
  adouble lastValue;    ///< keep the last value
  adouble lowValue;     ///< keep low (minimal) value of cache
  std::string filename; ///< name of cache unit file

public:
  CacheUnit() = default;
  /**
   * @brief CacheUnit
   * @param id
   */
  CacheUnit(int id, double value);

  /**
   * @brief CacheUnit just a copy constructor
   * @param copy is the object to copy
   */
  CacheUnit(const CacheUnit &copy);

  /**
   * @brief free fries file memory and delete cache file from file system
   */
  void free();

  void delete_();

  Array getAllContent();
  void writeRawData(double *arr, size_t size);

  /**
   * @brief operator = is used to swap values, DON'T delete twince
   * @param other is the value to copu
   * @return a reference to this value
   */
  CacheUnit &operator=(const CacheUnit &other);

  /**
   * @brief operator > used for autosorting system
   * @param value is the current value to add to cache
   * @return true if lastValue is bigger then value
   */
  bool operator>(double value);

  /**
   * @brief setLastValue sets the lastValue value and write it to file
   * @param value is the value to cache
   */
  void setLastValue(double value);

  /**
   * @brief setLowValue sets the lowValue value and write it to reverse file
   * @param value is the value to cache
   */
  void setLowValue(double value);

  /**
   * @brief switchToReadMode by default the cache is opened for write
   */
  void switchToReadMode();

  void prepareForMergeSort();

  /**
   * @brief readValue read the next value from cache
   * @return false if no ore values in cache, otherwise true
   */
  bool readValue();

  /**
   * @brief getLastValue gets the last readed value
   */
  double getLastValue();

  /**
   * @brief getLowValue gets the minimal value of cache
   * @return the minimal value of cache
   */
  double getLowValue();
};

class Sorter {
public:
  ~Sorter();

  /**
   * @brief load loads content of file to cache
   * @param in is the file to read from
   */
  void load(std::ifstream &in);

  void sort();

  /**
   * @brief write writes content of cache to file
   * @param outis the file to write in
   */
  void write(std::ofstream &out);

private:
  std::vector<CacheUnit> streams;

  size_t parts = 128;
};
