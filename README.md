CPU: Intel(R) Core(TM) i3-8130U CPU @ 2.20GHz (Чистота была программно снижена до 2.00GHz.)

SSD: INTEL SSDPEKKW128G8

```
[lixcode@ThinkpadE580 build-task-1---sorter-Clang]$ ./timeout -m 102400 -t 1800 ./task-1-sorter data1GB.in data1GB.out
Loading...
Sorting... 0 of 128
Sorting... 1 of 128
Sorting... 2 of 128
Sorting... 3 of 128
Sorting... 4 of 128
Sorting... 5 of 128
Sorting... 6 of 128
Sorting... 7 of 128
Sorting... 8 of 128
Sorting... 9 of 128
Sorting... 10 of 128
Sorting... 11 of 128
Sorting... 12 of 128
Sorting... 13 of 128
Sorting... 14 of 128
Sorting... 15 of 128
Sorting... 16 of 128
Sorting... 17 of 128
Sorting... 18 of 128
Sorting... 19 of 128
Sorting... 20 of 128
Sorting... 21 of 128
Sorting... 22 of 128
Sorting... 23 of 128
Sorting... 24 of 128
Sorting... 25 of 128
Sorting... 26 of 128
Sorting... 27 of 128
Sorting... 28 of 128
Sorting... 29 of 128
Sorting... 30 of 128
Sorting... 31 of 128
Sorting... 32 of 128
Sorting... 33 of 128
Sorting... 34 of 128
Sorting... 35 of 128
Sorting... 36 of 128
Sorting... 37 of 128
Sorting... 38 of 128
Sorting... 39 of 128
Sorting... 40 of 128
Sorting... 41 of 128
Sorting... 42 of 128
Sorting... 43 of 128
Sorting... 44 of 128
Sorting... 45 of 128
Sorting... 46 of 128
Sorting... 47 of 128
Sorting... 48 of 128
Sorting... 49 of 128
Sorting... 50 of 128
Sorting... 51 of 128
Sorting... 52 of 128
Sorting... 53 of 128
Sorting... 54 of 128
Sorting... 55 of 128
Sorting... 56 of 128
Sorting... 57 of 128
Sorting... 58 of 128
Sorting... 59 of 128
Sorting... 60 of 128
Sorting... 61 of 128
Sorting... 62 of 128
Sorting... 63 of 128
Sorting... 64 of 128
Sorting... 65 of 128
Sorting... 66 of 128
Sorting... 67 of 128
Sorting... 68 of 128
Sorting... 69 of 128
Sorting... 70 of 128
Sorting... 71 of 128
Sorting... 72 of 128
Sorting... 73 of 128
Sorting... 74 of 128
Sorting... 75 of 128
Sorting... 76 of 128
Sorting... 77 of 128
Sorting... 78 of 128
Sorting... 79 of 128
Sorting... 80 of 128
Sorting... 81 of 128
Sorting... 82 of 128
Sorting... 83 of 128
Sorting... 84 of 128
Sorting... 85 of 128
Sorting... 86 of 128
Sorting... 87 of 128
Sorting... 88 of 128
Sorting... 89 of 128
Sorting... 90 of 128
Sorting... 91 of 128
Sorting... 92 of 128
Sorting... 93 of 128
Sorting... 94 of 128
Sorting... 95 of 128
Sorting... 96 of 128
Sorting... 97 of 128
Sorting... 98 of 128
Sorting... 99 of 128
Sorting... 100 of 128
Sorting... 101 of 128
Sorting... 102 of 128
Sorting... 103 of 128
Sorting... 104 of 128
Sorting... 105 of 128
Sorting... 106 of 128
Sorting... 107 of 128
Sorting... 108 of 128
Sorting... 109 of 128
Sorting... 110 of 128
Sorting... 111 of 128
Sorting... 112 of 128
Sorting... 113 of 128
Sorting... 114 of 128
Sorting... 115 of 128
Sorting... 116 of 128
Sorting... 117 of 128
Sorting... 118 of 128
Sorting... 119 of 128
Sorting... 120 of 128
Sorting... 121 of 128
Sorting... 122 of 128
Sorting... 123 of 128
Sorting... 124 of 128
Sorting... 125 of 128
Sorting... 126 of 128
Sorting... 127 of 128
Merging...
FINISHED CPU 605.95 MEM 13688 MAXMEM 13688 STALE 0 MAXMEM_RSS 10656
<time name="ALL">605960</time>'
```

Время CPU: 10 минут 5.95 секунд

Память: 13.7 MiB

Утилита: https://github.com/pshved/timeout
